<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => '7 Makanan Tradisional Khas Jawa yang cocok disajikan saat HUT RI 2020',
            'content' => 'Makanan khas Jakarta yang enak cocok disajikan saat HUT RI untuk memeriahkan perlombaan anak-anak.',
            'category' => 'HUT RI ke-75',
        ]);
    }
}
